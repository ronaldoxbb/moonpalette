﻿namespace MoonPalette
{
    partial class FormEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormEditor));
            this.panelInBox = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panelForeColor = new System.Windows.Forms.Panel();
            this.buttonRecolor = new System.Windows.Forms.Button();
            this.buttonAbrir = new System.Windows.Forms.Button();
            this.buttonSaveAs = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panelInBox
            // 
            this.panelInBox.AllowDrop = true;
            this.panelInBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelInBox.AutoScroll = true;
            this.panelInBox.BackColor = System.Drawing.Color.Gray;
            this.panelInBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelInBox.Location = new System.Drawing.Point(8, 24);
            this.panelInBox.Name = "panelInBox";
            this.panelInBox.Size = new System.Drawing.Size(744, 432);
            this.panelInBox.TabIndex = 0;
            this.panelInBox.DragDrop += new System.Windows.Forms.DragEventHandler(this.panelInBox_DragDrop);
            this.panelInBox.DragOver += new System.Windows.Forms.DragEventHandler(this.panelInBox_DragOver);
            this.panelInBox.Resize += new System.EventHandler(this.panelInBox_Resize);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(135, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Arraste as imagens para cá";
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.panelForeColor);
            this.panel2.Controls.Add(this.buttonRecolor);
            this.panel2.Controls.Add(this.buttonAbrir);
            this.panel2.Controls.Add(this.buttonSaveAs);
            this.panel2.Controls.Add(this.pictureBox1);
            this.panel2.Location = new System.Drawing.Point(8, 462);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(744, 58);
            this.panel2.TabIndex = 3;
            // 
            // panelForeColor
            // 
            this.panelForeColor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelForeColor.Location = new System.Drawing.Point(90, 4);
            this.panelForeColor.Name = "panelForeColor";
            this.panelForeColor.Size = new System.Drawing.Size(21, 21);
            this.panelForeColor.TabIndex = 6;
            this.panelForeColor.Click += new System.EventHandler(this.panelForeColor_Click);
            // 
            // buttonRecolor
            // 
            this.buttonRecolor.Location = new System.Drawing.Point(4, 3);
            this.buttonRecolor.Name = "buttonRecolor";
            this.buttonRecolor.Size = new System.Drawing.Size(80, 23);
            this.buttonRecolor.TabIndex = 5;
            this.buttonRecolor.Text = "&Recolorir";
            this.buttonRecolor.UseVisualStyleBackColor = true;
            this.buttonRecolor.Click += new System.EventHandler(this.buttonRecolor_Click);
            // 
            // buttonAbrir
            // 
            this.buttonAbrir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonAbrir.Location = new System.Drawing.Point(528, 3);
            this.buttonAbrir.Name = "buttonAbrir";
            this.buttonAbrir.Size = new System.Drawing.Size(103, 23);
            this.buttonAbrir.TabIndex = 5;
            this.buttonAbrir.Text = "&Abrir...";
            this.buttonAbrir.UseVisualStyleBackColor = true;
            this.buttonAbrir.Click += new System.EventHandler(this.buttonAbrir_Click);
            // 
            // buttonSaveAs
            // 
            this.buttonSaveAs.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSaveAs.Location = new System.Drawing.Point(637, 3);
            this.buttonSaveAs.Name = "buttonSaveAs";
            this.buttonSaveAs.Size = new System.Drawing.Size(103, 23);
            this.buttonSaveAs.TabIndex = 5;
            this.buttonSaveAs.Text = "&Salvar como...";
            this.buttonSaveAs.UseVisualStyleBackColor = true;
            this.buttonSaveAs.Click += new System.EventHandler(this.buttonSaveAs_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(306, 10);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(48, 48);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Visible = false;
            // 
            // folderBrowserDialog1
            // 
            this.folderBrowserDialog1.Description = "Diretório destino...";
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.DefaultExt = "png";
            this.saveFileDialog1.Filter = "PNG files|*.png";
            this.saveFileDialog1.Title = "Salvar como...";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.DefaultExt = "png";
            this.openFileDialog1.Filter = "PNG files|*.png";
            this.openFileDialog1.Multiselect = true;
            this.openFileDialog1.Title = "Abrir";
            // 
            // FormEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(760, 526);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panelInBox);
            this.Name = "FormEditor";
            this.Text = "MoonPalette";
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panelInBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button buttonSaveAs;
        private System.Windows.Forms.Button buttonRecolor;
        private System.Windows.Forms.Panel panelForeColor;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Button buttonAbrir;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
    }
}

