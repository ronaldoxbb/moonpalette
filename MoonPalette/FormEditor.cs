﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace MoonPalette
{
    public partial class FormEditor : Form
    {
        private Color newForeColor = Color.Black;

        public FormEditor()
        {
            InitializeComponent();
            panelForeColor.BackColor = newForeColor;
        }

        private string[] GetImageFiles(IDataObject data)
        {
            var fileNames = new List<string>();
            if (data.GetDataPresent(DataFormats.FileDrop))
            {
                var filesDrop = (string[])(data.GetData(DataFormats.FileDrop));
                foreach (var fn in filesDrop)
                    if (File.Exists(fn) && Path.GetExtension(fn).ToLower() == ".png")
                        fileNames.Add(fn);
            }
            return fileNames.ToArray();
        }

        private void panelInBox_DragOver(object sender, DragEventArgs e)
        {
            var imgFiles = GetImageFiles(e.Data);
            if (imgFiles.Length > 0)
                e.Effect = DragDropEffects.Copy;
            else
                e.Effect = DragDropEffects.None;
        }

        private void panelInBox_DragDrop(object sender, DragEventArgs e)
        {
            var imgFiles = GetImageFiles(e.Data);
            foreach (var fn in imgFiles)
                AddImageFile(fn);
            PackLayout();
        }

        private void PackLayout()
        {
            var lineHeight = 0;
            var y = 0;
            var x = 0;
            for (var i = 0; i < panelInBox.Controls.Count; i++)
            {
                var pic = (MyPicBox)panelInBox.Controls[i];
                if (x > 0 && x + pic.Width > panelInBox.ClientSize.Width)
                {
                    y += lineHeight;
                    x = 0;
                    lineHeight = 0;
                }
                pic.Location = new Point(x, y);
                x += pic.Width;
                lineHeight = Math.Max(lineHeight, pic.Height);
            }
        }

        class MyPicBox : PictureBox
        {
            public Bitmap OriginalImage;
            public string OriginalFileName;
        }

        private void AddImageFile(string fn)
        {
            Bitmap bmp;
            using (var read = new Bitmap(fn))
                bmp = new Bitmap(read);
            var pic = new MyPicBox();
            pic.OriginalImage = bmp;
            pic.OriginalFileName = fn;
            pic.Image = Recolor(new Bitmap(bmp));
            pic.BackgroundImage = pictureBox1.Image;
            pic.BackgroundImageLayout = ImageLayout.Tile;
            pic.SizeMode = PictureBoxSizeMode.AutoSize;
            pic.Padding = new Padding(4);
            pic.BorderStyle = BorderStyle.Fixed3D;
            panelInBox.Controls.Add(pic);
        }

        private Bitmap Recolor(Bitmap bmp)
        {
            if (newForeColor == Color.Black)
                return bmp;
            var bmp2 = new Bitmap(bmp);
            for (var y = 0; y < bmp.Height; y++)
                for (var x = 0; x < bmp.Width; x++)
                {
                    var pix = bmp.GetPixel(x, y);
                    var gray = (byte)(((int)pix.R + (int)pix.G + (int)pix.B) / (int)3);
                    var howBlack = (double)(255 - gray) / 255d;
                    var pix2 = Color.FromArgb(pix.A, (int)(newForeColor.R * howBlack), (int)(newForeColor.G * howBlack), (int)(newForeColor.B * howBlack));
                    bmp2.SetPixel(x, y, pix2);
                }
            return bmp2;
        }

        private void panelInBox_Resize(object sender, EventArgs e)
        {
            PackLayout();
        }

        private void RecolorAll()
        {
            for (var i = 0; i < panelInBox.Controls.Count; i++)
            {
                var pic = (MyPicBox)panelInBox.Controls[i];
                pic.Image = Recolor(pic.OriginalImage);
            }
        }

        private void buttonSaveAs_Click(object sender, EventArgs e)
        {
            var imgCount = panelInBox.Controls.Count;
            var imgList = new List<MyPicBox>();
            for (var i = 0; i < imgCount; i++)
                imgList.Add((MyPicBox)panelInBox.Controls[i]);

            if (imgCount > 1)
            {
                var originalFolder = Path.GetDirectoryName(imgList.First().OriginalFileName);
                folderBrowserDialog1.SelectedPath = originalFolder;
                if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
                {
                    if (folderBrowserDialog1.SelectedPath.Equals(originalFolder, StringComparison.OrdinalIgnoreCase))
                        if (MessageBox.Show("Sobrescrever os arquivos originais?", "Confirmação", MessageBoxButtons.OKCancel) != DialogResult.OK)
                            return;
                    SaveAll(imgList, folderBrowserDialog1.SelectedPath);
                }
            }
            else if (imgCount == 1)
            {
                var fileName = imgList.First().OriginalFileName;
                saveFileDialog1.FileName = fileName;
                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                    Save(imgList.First(), saveFileDialog1.FileName);

            }
        }

        private void Save(MyPicBox it, string destFileName)
        {
            var bitmap = new Bitmap(it.Image);
            using (var fileStream = new FileStream(destFileName, FileMode.Create))
                bitmap.Save(fileStream, ImageFormat.Png);
        }

        private void SaveAll(List<MyPicBox> them, string destFolder)
        {
            foreach (var it in them)
            {
                var destFileName = Path.Combine(destFolder, Path.GetFileName(it.OriginalFileName));
                Save(it, destFileName);
            }
            MessageBox.Show("Arquivos salvos.");
        }

        private void buttonRecolor_Click(object sender, EventArgs e)
        {
            RecolorAll();
        }

        private void panelForeColor_Click(object sender, EventArgs e)
        {
            colorDialog1.Color = newForeColor;
            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                newForeColor = colorDialog1.Color;
                panelForeColor.BackColor = newForeColor;
            }
        }

        private void buttonAbrir_Click(object sender, EventArgs e)
        {
            ShowOpenFileDialog();
        }

        private void ShowOpenFileDialog()
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                foreach (var fn in openFileDialog1.FileNames)
                    AddImageFile(fn);
                PackLayout();
            }
        }
    }
}
